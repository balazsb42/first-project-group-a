import pandas as pd
import sys
import nltk
from nltk import wordpunct_tokenize
from nltk.corpus import stopwords

def _calculate_languages_ratios(text):
    """
    This function takes the raw text in, tokenizes it and matches each word to a language

    @param text: Raw text, which language we want to detect
    @type text: str
    
    @return: Dictionary with languages and frequency of stopwords
    @rtype: dict
    """
    languages_ratios = {}

    tokens = wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]

    for language in stopwords.fileids():
        stopwords_set = set(stopwords.words(language))
        words_set = set(words)
        common_elements = words_set.intersection(stopwords_set)

        languages_ratios[language] = len(common_elements)

    return languages_ratios


def detect_language(text):
    """
    This method returns the language which was detected in the text the most times.

    @param text: Text whose language is wanted to be detected
    @type text: str
    
    @return: Language that was detected in the string the most times.
    @rtype: str
    """
    ratios = _calculate_languages_ratios(text)

    most_rated_language = max(ratios, key=ratios.get)

    return most_rated_language

if __name__=='__main__':
    filenames = ["amager", "indreby", "nørrebro", "østerbro", "torvehallerne", "valby", "vesterbro"]
    #filenames = ["østerbro", "torvehallerne", "valby", "vesterbro"]

    nltk.download("stopwords")

    # loop through all the regions and parse them into txt files with every english caption at new line
    for filename in filenames:
        # read the csv and archive only two collums, from which one was not used in the end; and transform captions to list 
        df = pd.read_csv("raw_instagram_data/" + filename + ".csv", dtype={"node.edge_media_to_caption.edges.0.node.text": str, "node.accessibility_caption": str}, usecols=["node.edge_media_to_caption.edges.0.node.text", "node.accessibility_caption"])
        df = df.astype({"node.edge_media_to_caption.edges.0.node.text": str, "node.accessibility_caption": str})
        captions = df["node.edge_media_to_caption.edges.0.node.text"].tolist()

        english_captions = []
        
        f = open("danish_insta_captions/" + filename + ".txt", "w+")

        for caption in captions:
            if detect_language(caption) == "danish":
                #print(caption.rstrip("\n\r"))
                f.write(caption.replace('\n', ' ').replace('\r', '') + "\n")
        f.close()

        print(filename + " has been completed")


