from newspaper import Article

filename = 'stopwordsDan.txt'	
with open(filename) as file:
	stopwords = []
	for i in file:
		stopwords.append(i.strip("\n"))

url = [
'https://www.selvsalg.dk/bolig/50926/frederikssundsvej_78a_st_th-2400-koebenhavn_nv?fbclid=IwAR2kbUolDuZK_xvnY_Qx2y5ZF-muUSPBnpozeL_rw8FAY1pgvTnqg_jlTIM',
'https://politiken.dk/indland/kobenhavn/art7000098/Cykelpigen-p%C3%A5-N%C3%B8rrebro-skal-d%C3%B8',
'https://gaffa.dk/nyhed/128068/distortion-er-klar-med-56-gadefester-pa-norrebro-og-vesterbro'
]

for site in url:
	article = Article(site, language ='da')
	article.download()
	article.parse()	
	article.nlp()

	keys = list(article.keywords)
	keywords = [x for x in keys if x not in stopwords]
	print(keywords[:1000])
