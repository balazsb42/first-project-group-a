from nltk import wordpunct_tokenize
from nltk.corpus import stopwords
from collections import Counter
from string import punctuation
from collections import defaultdict
import math
from nltk import tokenize
from string import punctuation
import nltk
import re
import csv
from nltk.util import ngrams

filenames = ["amager", "indreby", "nørrebro", "østerbro", "valby", "vesterbro"]
words_to_ommit = ["amager", "indreby", "nørrebro", "østerbro", "valby", "vesterbro", 'pdf', 'øst', 'projektskabelon', 'borgerhenvendelse', 'foreningen', 'lokaludvalget', 'vest', 'lokaludvalgets', 'opstillingsplan', 'kun', 'hvide', 'vedr', 'syd', "docx", 'projektskema', 'østre', 'ansøgningsrunde', 'vej', 'den', 'høringssvar', 'proces', 'månedens', 'nørre', 'sag', 'dagsorden', 'kommentar', 'midler', 'bevilling', 'projektbeskrivelse', 'tag', 'mv', 'ifm', 'avlu', 'midler', 'offentlig', 'skriftlig', 'ifbm', "by", 'udtalelse', 'mellem', 'gl', 'henvendelser', 'puls', "dag", 'høring']

def read_all_files(filenames):
#with open('word_count.csv', 'a') as csvFile:
    #writer =csv.writer(csvFile)
    #writer.writerow(['district','posts','words'])

    districts = {}

    for disctrict in filenames:
        with open("meeting_minutes/" + disctrict + ".txt") as f:
            minutes = ""
            minute = f.read()
            minute = minute.strip().replace('\n', ' ').replace('\r', '').replace('\t', '')
            minutes += "".join(minute)
            all_words = tokenize.wordpunct_tokenize(minutes.strip())
            stopwords = nltk.corpus.stopwords.words('danish')
            all_words = [w.lower().strip().rstrip(punctuation) for w in all_words if w not in stopwords]
            all_words = [word for word in all_words if re.search("[^\s]", word) and word.isalpha()]
            bgs = ngrams(all_words, 3)
            districts[disctrict] = Counter()
            #writer.writerow([disctrict,len(posts),len(words)])

            for word in bgs:
                i=0
                for part in word:
                    if part not in words_to_ommit:
                        i += 1
                if(i == len(word)):
                    districts[disctrict].update({word})

    #csvFile.close()
    return districts

def idf(tf_dict):
    word_appirance = defaultdict(int)
    for district in filenames:
        season_wordlist = list(tf_dict[district].keys())
        for word in season_wordlist:
            word_appirance[word] += 1
    idf = {}
    for key in word_appirance:
        idf[key] =math.log10(7/ word_appirance[key])

    return idf

tf = read_all_files(filenames)
idf = idf(tf)

def tf_idf(tf,idf):
    tf_idf = {}
    #with open('highvalue_tfidf.csv', 'a') as csvFile:
    #    writer = csv.writer(csvFile)
    #    writer.writerow(['word','tfidf_value','district'])

    for district in filenames:
        tf_idf[district] = Counter()
        for word,idf_value in idf.items():

            tf_value = tf[district][word]
            #if tf[district][word] > 0:
            #    tf_value =1+math.log10(tf[district][word])

            #else:
            #    tf_value = 1
            tf_idf[district][word] = (tf_value)*idf_value
#            writer.writerow([word,tf_idf[district][word],district])
    #csvFile.close()
    return tf_idf

tf_idf = tf_idf(tf,idf)
for district in filenames:
    with open("minutes_results/3_grams_tf_idf/" + district + ".csv", "w+") as f:
        f.write("words, tf-idf\n")
        for word in tf_idf[district].most_common(20):
            f.write(str(word[0][0]) + " " + str(word[0][1]) + " " + str(word[0][2]) + ", " + str(word[1]) + "\n")
