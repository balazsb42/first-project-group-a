import emoji
from collections import Counter
from nltk import tokenize
from string import punctuation
from nltk import FreqDist
import matplotlib.pyplot

word_tokenizer = tokenize.TreebankWordTokenizer()
counter = Counter()
exclude = set(punctuation)


def read_file(filename):
    with open(filename) as myfile:
        content = myfile.read()

    return content

def emoji_dictionary(listOftext):
    list_of_text = tokenize.sent_tokenize(listOftext)
    cleaned = [word.strip("\n").replace("#", "").replace(".", "").replace("@","") for word in list_of_text]
    dict_of_words = word_tokenizer.tokenize_sents(cleaned)
    words = [word for list_of_text in dict_of_words for word in list_of_text]

    return counter.most_common(10)



nature_emojis = read_file(emoji.demojize("emojis.txt"))
text_file = read_file(emoji.emojize("vesterbro.txt"))
emoj_dic = emoji_dictionary(text_file)

print(emoj_dic)
