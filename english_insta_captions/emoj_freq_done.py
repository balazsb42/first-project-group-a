import emoji

def file_reader(neighbourhoodcaptions):
    with open(neighbourhoodcaptions) as mf:
        list_of_text = [word.strip("\n") for word in mf]

    return list_of_text

def emoji_reader(list_of_emojis):
    my_list = []
    with open(list_of_emojis) as mf:
        for line in mf:
            my_list.append(emoji.emojize(line.strip("\n").strip("\t"))) #Cant do LC with emojis

    return my_list

def emoji_counter(list_of_captions, list_of_emojis):
    counter = 0
    for sentences in list_of_captions:
        for emojis in list_of_emojis:
            if emojis in sentences:
                counter += 1

    return counter

def total_captions(list_of_captions):
    counter = 0
    with open(list_of_captions) as myfile:
        for line in myfile:
            line.strip("\n")
            counter += 1

    return counter

# EMOJIS
nature_emojis = emoji_reader('natureemojis.txt')
food_emojis = emoji_reader('foodemojis.txt')
social_emojis = emoji_reader('socialemojis.txt')

# TEXTFILES FROM NEIGHBOURHOODS
torvehallerne = file_reader('torvehallerne.txt')
amager = file_reader('amager.txt')
noerrebro = file_reader('nørrebro.txt')
vesterbro = file_reader('vesterbro.txt')
valby = file_reader('valby.txt')
indreby = file_reader('indreby.txt')


print(emoji_counter(noerrebro, nature_emojis))
print(emoji_counter(noerrebro, food_emojis))
print(emoji_counter(noerrebro, social_emojis))
print(total_captions('nørrebro.txt'))
