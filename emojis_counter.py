# this code counts the frequency for each individual emoji in a text file
# furthermore, it registers and prints the most common emojis that is controlled
# via the integer one inputs in the shell

import sys
import emoji
import collections
from collections import defaultdict
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

your_file = open('english_insta_captions/' + sys.argv[1]).read()

def extract_emojis(str):
    return [c for c in str if c in emoji.UNICODE_EMOJI]

my_emojis = extract_emojis(your_file)


emoji_count = defaultdict(int)

for e in my_emojis:
    emoji_count[emoji.demojize(e)] += 1

#print(emoji_count)


# Print most common emoji
n_print = int(input("How many most common emojis to print: "))
print("\nOK. The {} most common emojis are as following\n".format(n_print))

emoji_counter = collections.Counter(emoji_count)

for my_emojis, emoji_count in emoji_counter.most_common(n_print):
    print(my_emojis,':' ,emoji_count)


# Create a data frame of the most common emojis
# Draw a bar chart
lst = emoji_counter.most_common(n_print)
df = pd.DataFrame(lst, columns = ['Emoji', 'Count'])
df.plot.barh(x = 'Count', y = 'Emoji', )
plt.xlabel('Emoji', fontsize = 8)
plt.ylabel('Count', fontsize = 8)
plt.yticks(fontsize = 5, rotation = 25)
plt.savefig('plots_emoji/' + 'emoji_top_{}'.format(n_print) + '_' + sys.argv[2] + '.png', dpi = 100)
