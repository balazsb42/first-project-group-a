19-årig mand og to tilfældige ramt af skud på Nørrebro
Københavns Politi formoder, at den 19-årige og hans kammerat var målet for et nyt banderelateret skyderi.
Tre personer blev fredag aften ramt af skud under et nyt skyderi i København.

Blandt de ramte er to tilfældige mænd på omkring 50 år, der befandt sig i hver deres bil.

Den ene blev ramt i armen og den anden i skulderen. De er ikke i kritisk tilstand, oplyser Københavns Politi i en pressemeddelelse.

Derudover blev en 19-årig mand ramt under venstre arm. Mandens tilstand betegnes som kritisk, men stabil. Han er uden for livsfare, oplyser Københavns Politi tidligt lørdag morgen.


»Umiddelbart er det vores vurdering, at skyderiet har relation til den verserende konflikt, der har udspillet sig over den seneste uge«, udtaler politiinspektør Torben Svarrer i en meddelelse.

»Det er vores foreløbige vurdering, at den 19-årige og dennes 22-årige kammerat var målet for skyderiet, men det skal vi have efterforsket nærmere«, tilføjer han.

Københavns Politi opfordrer folk til at ringe 114, hvis de har set eller hørt noget, der kan være relevant for efterforskningen.

Skyderiet skete i Meinungsgade og Guldbergsgade på Nørrebro.

Betjente var til stede i området til en anden opgave klokken 21.09, hvor de hørte skud.

Foreløbigt vurderer Københavns Politi, at der var tale om flere gerningsmænd.

»De flygtede fra Guldbergsgade til fods efter skyderiet«, siger efterforskningsleder Søren Lauritsen. Han tilføjer, at gerningsmændene flygtede i retning mod Sankt Hans Torv.

Det har endnu ikke været muligt at få et signalement af de formodede gerningsmænd.

Skyderi afstumpet
Politidirektør Anne Tønnes kalder fredagens skyderi »afstumpet«.

»Det er et udtryk for en meget afstumpet voldsparathed, når der bliver skudt på åben gade. Men vi er stålsatte og sætter hårdt ind med massiv tilstedeværelse, så vi kan få stoppet det her bandeuvæsen«, siger hun.

Det er den skudepisode i og omkring København den seneste uge, og med fredagens skyderi er syv personer blevet ramt af skud.

Tidligere på ugen etablerede Københavns Politi en visitationszone på Nørrebro.

Det betyder, at politiet har ret til at visitere personer og biler, selv om det ikke har en konkret mistanke mod en person.

»Jeg kan godt forstå, hvis borgerne på Nørrebro føler sig utrygge. Derfor vil de også opleve en omfattende polititilstedeværelse. Vi gør alt, hvad vi kan for at sikre trygheden«, siger Anne Tønnes.

Politidirektøren oplyste tidligere på ugen, at de mange skyderier skyldes, at en bande er splittet op.

Det skulle ifølge flere medier dreje sig om banden Brothas.