def hash_count(filename, word):
    words = ','.join(word)
    count = dict((words, 0) for w in word)
    with open(filename, 'r') as file:
    #fifi = [x.strip("\n").replace(' '' ', '') for x in f]
        for line in file:
        #    for word in words:
            where = line.find('#' + word)
            if where == -1:
                continue
#                after = line[where + len(word) + 1]
#                acceptable_chars = ['n,a,t,u,r,e']
#                if after in acceptable_chars:
#                    continue
            count[words] += 1
    return count

amager = hash_count('amager.txt', 'nature')
print('Total hashtags:', amager)

valby = hash_count('valby.txt', 'nature')
print('Total hashtags:', valby)

norrebro = hash_count('nørrebro.txt', 'nature')
print('Total hashtags:', norrebro)

osterbro = hash_count('østerbro.txt', 'nature')
print('Total hashtags:', osterbro)

vesterbro = hash_count('vesterbro.txt', 'nature')
print('Total hashtags:', vesterbro)

indreby = hash_count('indreby.txt', 'nature')
print('Total hashtags:', indreby)

torve = hash_count('torvehallerne.txt', 'nature')
print('Total hashtags:', torve)
