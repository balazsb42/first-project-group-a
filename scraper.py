import requests
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup

API_REQUEST_URL = 'https://home.dk/umbraco/backoffice/home-api/SEARCH?CurrentPageNumber=1&SearchResultsPerPage=100&q=K%C3%B8benhavn%20kommune&Energimaerker=null&SearchType=0&_=1551192565174'
TOTAL_SEARCH_RESULTS = 0
HOUSING_DATA = []
FETCH_URLS = []
SEARCH_PER_PAGE = 100
PAGE_NUMBER = 0

def search(page_number):
    resp = requests.get(url='https://home.dk/umbraco/backoffice/home-api/SEARCH?CurrentPageNumber='+ str(page_number) +'&SearchResultsPerPage='+ str(SEARCH_PER_PAGE) +'&q=K%C3%B8benhavn%20kommune&Energimaerker=null&SearchType=0&_=1551192565174').json()
    search_results = resp['searchResults']

    search_results_length = len(search_results)
    for item in search_results:
        print(item['city'])
        FETCH_URLS.append(dict(area=item['city'], url=item['boligurl']))
    return search_results_length

def fetch_housing_data_from_html(url):
    raw_html = simple_get(url)
    html = BeautifulSoup(raw_html, 'html.parser')
    data_div = html.find_all(class_='inside')
    for i in html.find_all(class_='inside'):
        return i.get_text()

def get_total_number(url):
    resp = requests.get(url=url).json()
    return resp

def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


def log_error(e):
    """
    It is always a good idea to log errors.
    This function just prints them, but you can
    make it do anything.
    """
    print(e)

response = get_total_number(API_REQUEST_URL)
TOTAL_SEARCH_RESULTS = response['totalSearchResults']

while TOTAL_SEARCH_RESULTS > 0:
    print(TOTAL_SEARCH_RESULTS)
    search_results_length = search(PAGE_NUMBER)
    PAGE_NUMBER += 1
    TOTAL_SEARCH_RESULTS -= search_results_length

FETCH_COUNTER = 0
f = open('scrape_results.txt', 'a')
for item in FETCH_URLS:
    if(item['url'].endswith('.aspx')):
        FETCH_COUNTER += 1
        print('Writing data: ' + item['url'])
        f.write('\n')
        f.write('AREA: ' + item['area'])
        f.write('\n')
        data = fetch_housing_data_from_html(item['url'])
        f.write('DATA: ' + data.replace('\n', ''))
        f.write('\n')
        f.write('---------------------------------------------------------------------------------------------------------------------------')
f.close()
