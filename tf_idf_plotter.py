import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rcParams

districts = ["amager", "indreby", "nørrebro", "østerbro", "torvehallerne", "valby", "vesterbro"]

for district in districts:
    rcParams.update({'figure.autolayout': True})
    ln_df = pd.read_csv('lntop20_'+district+'_tfidf.csv')
    nn_df = pd.read_csv('nntop20_'+district+'_tfidf.csv')
    ln_df.set_index('word', inplace=True, drop=True)
    nn_df.set_index('word', inplace=True, drop=True)

    fig,ax = plt.subplots()
    fig=plt.gcf()
    fig.set_size_inches(18.5, 10.5)
    ln_df.plot.bar(rot=90,subplots = True, ax=ax)



    plt.savefig('Insta_tf-idf_plots/ln_'+district+'.png')

    fig,ax = plt.subplots()
    fig=plt.gcf()
    fig.set_size_inches(18.5, 10.5)

    nn_df.plot.bar(rot=90,subplots = True, ax=ax)

    plt.savefig('Insta_tf-idf_plots/nn_'+district+'.png')
