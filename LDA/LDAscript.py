from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models
import gensim
import re

tokenizer = RegexpTokenizer(r'\w+')

# create danishstop words list
da_stop = get_stop_words('da')

# Create p_stemmer of class PorterStemmer
p_stemmer = PorterStemmer()
    
# compile sample documents into a list
doc_set = 'østerbro.txt'

# list for tokenized documents in loop
texts = []

# loop through document list
with open(doc_set) as file:
	for i in file:
	    
	    # clean and tokenize document string
	    raw = i.lower()
	    tokens = tokenizer.tokenize(raw)

	    # remove stop words from tokens
	    stopped_tokens = [i for i in tokens if not i in da_stop]
	    
	    # stem tokens
	    stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]

	    all_words = [word for word in stemmed_tokens if re.search("[^\s]", word) and word.isalpha()]
	    
	    # add tokens to list
	    texts.append(all_words)

# turn our tokenized documents into a id <-> term dictionary
dictionary = corpora.Dictionary(texts)
    
# Next, our dictionary must be converted into a bag-of-words:
# The doc2bow() function converts dictionary into a bag-of-words. 
corpus = [dictionary.doc2bow(text) for text in texts]

# generate LDA model
# num_topics: An LDA model requires the user to determine how many topics should be generated.
# passes: The number of laps the model will take through corpus. The greater the number of passes, the more accurate the model will be. 
ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=50, id2word = dictionary)

print(ldamodel.print_topics(num_topics=50, num_words=3))







