import emoji
import re
from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models
import gensim
import os

EN_DIR_BASE = './captions_english/'
DN_DIR_BASE = './captions_danish/'

en_stop = get_stop_words('en')
p_stemmer = PorterStemmer()
print('launches first')
en_stop.extend(['copenhagen','benhavn','amager','ama','denmark','valbi','valby','us','k'
    's','l','danmark','cph','r','indrebi','visitkph','visitcopenhagen','rrebro', 't', 'd', 'e',
    'kbh', 'torvehallern', 'n','sterbro', 'vesterbro', 'danish', 'o'])

def read_file(file_path):
    f = open(file_path, 'r')
    with f as file:
        data = '  '.join([line.replace('\n', '') for line in file.readlines()])
    f.close()
    return data
def clean_hashtags(raw):
    return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ",raw).split())
def get_dir_content():
    return os.listdir("./captions_english")

english_dir = get_dir_content()

for location in english_dir:
    print('PROCESSING FILE: ' + location)
    texts = []
    raw_string = read_file(EN_DIR_BASE + location)
    cleaned = clean_hashtags(raw_string).lower()
    tokens = tokenizer.tokenize(cleaned)
    stopped_tokens = [word for word in tokens if not word in en_stop]
    stemmed_tokens = [p_stemmer.stem(word) for word in stopped_tokens]
    texts.append(stemmed_tokens)
    dictionary = corpora.Dictionary(texts)
    corpus = [dictionary.doc2bow(text) for text in texts]
    print('BUILDING LDA_MODEL!')
    ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=5, id2word=dictionary, passes=100, minimum_probability=0)
    print('FINDINGS: \n')
    print(ldamodel.print_topics(num_topics=5, num_words=5))
    print('TOPIC SCORES: \n')
    print(ldamodel[corpus[0]])
    print('---------------------------------------------------------')
    f = open('./findings_en.txt', 'a')
    f.write('PROCESSED FILE: ' + location +'\n')
    f.write('LDA TOPICS: \n')
    f.write(str(ldamodel.print_topics(num_topics=5, num_words=5))+'\n')
    f.write('TOPIC SCORES: \n')
    f.write(str(ldamodel[corpus[0]])+'\n')
    f.write('---------------------------------------------------------\n')
    f.close()
"""raw_string = read_file('../english_insta_captions/amager.txt')
cleaned = clean_hashtags(raw_string).lower()

tokens = tokenizer.tokenize(cleaned)
stopped_tokens = [i for i in tokens if not i in en_stop]
stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]
texts = []
texts.append(stemmed_tokens)
dictionary = corpora.Dictionary(texts)

corpus = [dictionary.doc2bow(text) for text in texts]
ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=5, id2word = dictionary, passes=100, minimum_probability=0)
lda_corpus = ldamodel[corpus]

print(ldamodel.print_topics(num_topics=10, num_words=4))
print(ldamodel.top_topics(corpus=corpus))
print(ldamodel[corpus[0]])
"""
#for i in range(0,10):
#    print(ldamodel[corpus[i]])
