import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

word_df = pd.read_csv('word_count.csv')

word_df.set_index('district', inplace=True, drop=True)

word_df['words'].div(word_df.posts, axis=0).plot.bar(rot=0)


plt.savefig('words_per_post.png')
