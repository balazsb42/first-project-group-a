import sys
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk import tokenize
from string import punctuation
import nltk
  
nltk.download("vader_lexicon")
nltk.download("punkt")
nltk.download('stopwords')

sid = SentimentIntensityAnalyzer()

filenames = ["nørrebro1"]
#, "indreby", "nørrebro", "østerbro", "torvehallerne", "valby", "vesterbro"]

for filename in filenames:
    i = 0
    j = 0
    neg_captions = ""

    with open("english_newspaper_captions/" + filename + ".txt") as f:
        for caption in f.readlines():
            j += 1
            ss = sid.polarity_scores(caption)
            if ss["neg"] > 0.2:
                neg_captions += caption
                i += 1
    f.close()

    all_words = tokenize.wordpunct_tokenize(neg_captions)
    all_word_dist = nltk.FreqDist(w.lower().strip().rstrip(punctuation) for w in all_words)
    stopwords = nltk.corpus.stopwords.words('english')
    word_dist_without_stopwords = nltk.FreqDist(w.lower().strip().rstrip(punctuation) for w in all_words if w not in stopwords)

    print("{} negative captions from {} in total for {}.".format(i, j, filename))

    most_common = all_word_dist.most_common(20)
    most_common_without_stopwords = word_dist_without_stopwords.most_common(20)

    print()

    print("Most common without stopwords:")
    for word in most_common_without_stopwords:
        print(word)
    print("______________________________")
    print()

