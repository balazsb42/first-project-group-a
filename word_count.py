from nltk import wordpunct_tokenize
from nltk.corpus import stopwords
from collections import Counter
from string import punctuation
from collections import defaultdict
import math
import emoji
import csv


filenames = ["amager", "indreby", "nørrebro", "østerbro", "torvehallerne", "valby", "vesterbro"]

def read_all_files(filenames):
#with open('word_count.csv', 'a') as csvFile:
    #writer =csv.writer(csvFile)
    #writer.writerow(['district','posts','words'])

    stopWords = set(stopwords.words('english'))
    districts = {}

    for disctrict in filenames:
        with open("english_insta_captions/" + disctrict + ".txt") as f:

            raw_text = f.read()
            tokens = wordpunct_tokenize(raw_text)
            words = [word.lower().rstrip(punctuation) for word in tokens]
            posts = raw_text.split('\n')
            districts[disctrict] = Counter()
            #writer.writerow([disctrict,len(posts),len(words)])

            for word in words:

                if word not in stopWords and word != '' :
                    word = emoji.demojize(word)
                    districts[disctrict].update({word})

    #csvFile.close()
    return districts

def idf(tf_dict):
    word_appirance = defaultdict(int)
    for district in filenames:

        season_wordlist = list(tf_dict[district].keys())
        for word in season_wordlist:
            word_appirance[word] += 1
    print(word_appirance['amager'])
    idf = {}
    for key in word_appirance:
        idf[key] =math.log10(7/ float(word_appirance[key]))

    return idf

def tf_idf(tf,idf):
    tf_idf = {}
    #with open('highvalue_tfidf.csv', 'a') as csvFile:
    #    writer = csv.writer(csvFile)
    #    writer.writerow(['word','tfidf_value','district'])

    for district in filenames:
        tf_idf[district] = Counter()
        for word,idf_value in idf.items():

            #tf_value = tf[district][word]
            if tf[district][word] > 0:
                tf_value =1+math.log10(tf[district][word])

            else:
                tf_value = 1
            tf_idf[district][word] = (tf_value)*idf_value
#            writer.writerow([word,tf_idf[district][word],district])
#csvFile.close()
    return tf_idf
tf = read_all_files(filenames)
print('amager' in tf['amager'].keys())
idf = idf(tf)
#tf_idf = tf_idf(tf,idf)


#for district in filenames:
#    with open('lntop20_'+district+'_tfidf.csv', 'a') as csvFile:
#        writer = csv.writer(csvFile)
#        writer.writerow(['word','tfidf_value'])
#        for word,num in tf_idf[district].most_common(20):
#            writer.writerow([word,num])
