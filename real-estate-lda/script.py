import re
from collections import OrderedDict
from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models
import gensim

def lda_script(location_data, location):
    print('LDA LOCATION: ', location)
    for element in location_data:
        raw = element.lower()
        tokens = tokenizer.tokenize(raw)
        stopped_tokens = [i for i in tokens if not i in da_stop]
        stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]
        all_words = [word for word in stemmed_tokens if re.search("[^\s]", word) and word.isalpha()]
        texts.append(all_words)
    dictionary = corpora.Dictionary(texts)
    corpus = [dictionary.doc2bow(text) for text in texts]
    ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=50, id2word = dictionary)
    index_list = [x for x, y in ldamodel[corpus[0]]]
    file = open('results.txt', 'a')
    file.write('LOCATION: ' + location + '\n')
    file.write('\n')
    file.write(str(ldamodel.print_topics(num_topics=50, num_words=3)) + '\n')
    file.write(str(index_list) + '\n')
    file.close()

    print('HIGH WEIGHT TOPICS:')
    print(ldamodel[corpus[0]])
    print(index_list)

new_dict= {}
tokenizer = RegexpTokenizer(r'\w+')
da_stop = get_stop_words('da')
p_stemmer = PorterStemmer()
texts = []
with open('data.txt', 'r') as file:
    data  = ' '.join(file.readlines())
    file.close()

proov = re.findall('(?<=AREA:)(.*?)(?=---------------------------------------------------------------------------------------------------------------------------)', data, flags=re.S)
CLEARED_DATA = []

for item in proov:
    index = item.index('DATA')
    location = item[0:index]
    data = item[(index + len('DATA:'))::]
    tuple_data = (location.strip(), data.strip())
    CLEARED_DATA.append(tuple_data)

d = OrderedDict()
for k, *v in CLEARED_DATA:
    d.setdefault(k, []).append(v)
location_keys = list(d.keys())

for location in location_keys:
    #print(location)
    new_dict[location] = []
    for item in d[location]:
        new_dict.get(location).append(item[0])
    #print(len(d[location]))
#print(new_dict.get('Nordhavn'))
location_data = new_dict.get('Nordhavn')
for item in location_keys:
    lda_script(new_dict.get(item), item)
