import pandas as pd
import matplotlib.pyplot as plt

districts = {"amager", "indreby", "nørrebro", "østerbro", "torvehallerne", "valby", "vesterbro"}

for district in districts:

    top_20 = pd.read_csv('emoji_top_20_'+district+'.csv')
    top_20.set_index('emoji', inplace = True, drop = True)
    fig, ax = plt.subplots()
    ln_df.plot.bar(rot = 90, subplots = True, ax = ax)
    fig.tight_layout()
    plt.savefig('Insta_emoji_plots_'+district+'.png')


"""# Create a data frame of the most common emojis
# Draw a bar chart
lst = emoji_counter.most_common(n_print)
df = pd.DataFrame(lst, columns = ['Emoji', 'Count'])
df.plot.bar(x = 'Emoji', y = 'Count')
plt.savefig('emoji_plots/'+district+'.png')"""
