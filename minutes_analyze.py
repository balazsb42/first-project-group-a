import sys
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk import tokenize
from string import punctuation
import nltk
import re
from sklearn.feature_extraction.text import TfidfVectorizer

def most_common_words(tokenized_text, district):
    word_dist_without_stopwords = nltk.FreqDist(tokenized_text)
    most_common = word_dist_without_stopwords.most_common(20)
    with open("minutes_results/words/" + filename + ".txt", "w+") as f:
        for word in most_common:
            f.write(str(word[0]) + " " + str(word[1]))
            f.write("\n")
    f.close()


def most_common_bigrams(tokenized_text, district):
    bgs = nltk.bigrams(all_words)
    bigrams_dist_without_stopwords = nltk.FreqDist(bgs)
    most_common = bigrams_dist_without_stopwords.most_common(20)
    with open("minutes_results/bigram/" + filename + ".txt", "w+") as f:
        for word in most_common:
            f.write(str(word[0]) + " " + str(word[1]))
            f.write("\n")
    f.close()

filenames = ["amager", "indreby", "nørrebro", "østerbro", "valby", "vesterbro"]

for filename in filenames:
    minutes = ""
    with open("meeting_minutes/" + filename + ".txt") as f:
        for minute in f.readlines():
            minute = minute.strip().replace('\n', ' ').replace('\r', '').replace('\t', '')
            minutes += "".join(minute)
    f.close()

    all_words = tokenize.wordpunct_tokenize(minutes.strip())
    stopwords = nltk.corpus.stopwords.words('danish')
    all_words = [w.lower().strip().rstrip(punctuation) for w in all_words if w not in stopwords]
    all_words = [word for word in all_words if re.search("[^\s]", word) and word.isalpha()]
    most_common_words(all_words, filename)
    most_common_bigrams(all_words, filename)

